<?php

namespace EspiralApp;

abstract class EspiralApp {
	public static $apiKey = '';
	public static $apiBase = '';
	public static $apiBaseCart = '';
	public static $apiVersion = '';
	public static $version = '';

	public static function init(): void {
		$config            = Environment::getEnvironment();
		self::$version     = $config['version'];
		self::$apiVersion  = $config['apiVersion'];
		self::$apiBase     = $config['apiBase'];
		self::$apiBaseCart = $config['apiBaseCart'];
	}

	public static function setApiBase( $apiBase ): void {
		self::$apiBase = $apiBase;
	}

	public static function getApiBase(): string {
		return self::$apiBase;
	}

	public static function setApiBaseCart( $apiBaseCart ): void {
		self::$apiBaseCart = $apiBaseCart;
	}

	public static function getApiBaseCart(): string {
		return self::$apiBaseCart;
	}
}
