<?php

namespace EspiralApp;

use Exception;

class Cart extends EspiralAppResource3D {
	/**
	 * @throws Exception
	 */
	public function sign(
		$cardHolder,
		$address,
		$transaction,
		$linkDetails,
		$webhook,
		$metaData
	) {
		$body = array_merge(
			$cardHolder,
			$address,
			$transaction,
			$linkDetails,
			$webhook,
			$metaData
		);

		return parent::createSign( 'payOrder?key=' . User::$apiKey, $body );
	}
}
