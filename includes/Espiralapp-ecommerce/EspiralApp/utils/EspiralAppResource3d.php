<?php

namespace EspiralApp;

use Exception;

class EspiralAppResource3D {
	/**
	 * @throws Exception
	 */
	protected function createSign( $url, $body ) {
		$request_3_d = new Request3D();
		$request_3_d->setHeaders( array(
			"Content-Type: application/json",
			"cache-control: no-cache"
		) );
		$body = array_merge( $body );
		return $request_3_d->request( 'POST', $url, null, $body );
	}
}