<?php

namespace EspiralApp;

class Environment {
	private static $env;

	public static function setEnvironment( $mode = '' ): void {
		switch ( $mode ) {
			case 'develop':
				self::$env = [
					'version'     => '1.0.0',
					'apiVersion'  => '1.0.0',
					'apiBase'     => 'https://transaction.espiralapp.com/api/v2/',
					'apiBaseCart' => 'https://cart.espiralapp.com/',
					'apiBaseCartReact' => 'https://cart.espiralapp.com/'
				];
				break;
			case 'developLocal':
				self::$env = [
					'version'          => '0.0.1',
					'apiVersion'       => '1.0.44',
					'apiBase'          => 'http://transaction-api:8080/api/v2/',
					// 'apiBaseCart' => 'http://shopping-cart:3000/',
					'apiBaseCart'      => 'http://localhost:3005/',
					'apiBaseCartReact' => 'http://localhost:3001/'
				];
				break;
			default:
				self::$env = [
					'version'     => '1.0.0',
					'apiVersion'  => '1.0.0',
					'apiBase'     => 'https://transaction.espiralapp.com/api/v2/',
					'apiBaseCart' => 'https://cart.espiralapp.com/',
					'apiBaseCartReact' => 'https://cart.espiralapp.com/'
				];
				break;
		}
	}

	public static function getEnvironment(): array {
		if ( ! isset( self::$env ) ) {
			self::setEnvironment();
		}

		return self::$env;
	}
}
