<?php

use EspiralApp\Cancel;
use EspiralApp\Cart;
use EspiralApp\Environment;
use EspiralApp\EspiralApp;
use EspiralApp\User;

class WC_Espiralapp_3dsecure_Payment extends WC_Payment_Gateway {

	public function __construct() {

		$this->id                 = 'belugapay3dsecurepayment';
		$this->icon               = '';
		$this->has_fields         = true;
		$this->method_title       = __( 'Espiral 3d Secure Payment', 'belugapay3dsecurepayment' );
		$this->method_description = __( 'Allow payments by credit or debit card, visa or mastercard that are allowed in Mexican' );
		$this->enabled            = false;
		$this->supports           = array( 'products', 'refunds' );

		// Load the settings.
		$this->init_form_fields();
		$this->init_settings();

		// Define user set variables
		$this->title        = $this->get_option( 'title' );
		$this->description  = $this->get_option( 'description' );
		$this->instructions = $this->get_option( 'instructions', $this->description );

		$this->sandboxApiKey     = $this->settings['sandboxApiKey'];
		$this->productionApiKey  = $this->settings['productionApiKey'];
		$this->enabledProduction = $this->settings['enabledProduction'];
		$this->redirectSuccess   = $this->settings['redirectSuccess'];

		// Actions
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array(
			$this,
			'process_admin_options'
		) );
		add_action( 'woocommerce_thankyou_' . $this->id, array( $this, 'thankyou_page' ) );
		add_action( 'woocommerce_api_espiralresponsedata', array( $this, 'callback_handler_espiralresponsedata' ) );

		// Customer Emails
		add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );
	}

	function callback_handler_espiralresponsedata(): bool {
		$_POST = json_decode( file_get_contents( 'php://input' ), true );

		global $woocommerce;
		$order = new WC_Order( sanitize_text_field( (string) $_POST['request']['metadata']['idOrder'] ) );
		$order->payment_complete();
		// error_log( print_r( 'request: ' . json_encode( $_POST['request']['metadata']['idOrder'] ), true ) );

		$woocommerce->cart->empty_cart();

		update_post_meta( $order->get_id(), 'transactionId', sanitize_text_field( (string) $_POST['response']['id'] ) );
		update_post_meta( $order->get_id(), 'reference', sanitize_text_field( (string) $_POST['response']['reference'] ) );
		update_post_meta( $order->get_id(), 'authCode', sanitize_text_field( (string) $_POST['response']['authorizationNumber'] ) );

		$order->add_order_note( sprintf(
			"Transaction Id: %s,<br/>Reference: %s,<br/>Authorization Code: %s",
			sanitize_text_field( (string) $_POST['response']['id'] ),
			sanitize_text_field( (string) $_POST['response']['reference'] ),
			sanitize_text_field( (string) $_POST['response']['authorizationNumber'] )
		) );

		return true;
	}

	/**
	 * Initialize Gateway Settings Form Fields
	 */
	public function init_form_fields() {
		$this->form_fields = array(
			'enabled' => array(
				'title'   => __( 'Enable/Disable', 'wc-gateway-espiralapp' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable Espiral Payment', 'wc-gateway-espiralapp' ),
				'default' => 'no'
			),

			'title' => array(
				'title'       => __( 'Title', 'wc-gateway-espiralapp' ),
				'type'        => 'text',
				'description' => __( 'This controls the title for the payment method the customer sees during checkout.', 'wc-gateway-espiralapp' ),
				'default'     => __( 'Card Payment', 'wc-gateway-espiralapp' ),
				'desc_tip'    => true,
			),

			'enabledProduction' => array(
				'title'   => __( 'Enable Production', 'wc-gateway-espiralapp' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable API KEY Production', 'wc-gateway-espiralapp' ),
				'default' => 'no'
			),

			'sandboxApiKey' => array(
				'title'   => __( 'Sandbox API KEY', 'wc-gateway-espiralapp' ),
				'type'    => 'password',
				'default' => __( '', 'wc-gateway-espiralapp' )
			),

			'productionApiKey' => array(
				'title'   => __( 'Production API KEY', 'wc-gateway-espiralapp' ),
				'type'    => 'password',
				'default' => __( '', 'wc-gateway-espiralapp' )
			),

			'redirectSuccess' => array(
				'title'       => __( 'Success page URL', 'wc-gateway-espiralapp' ),
				'type'        => 'text',
				'description' => __( 'URL to redirect the customer when the order is paid.', 'wc-gateway-espiralapp' ),
				'default'     => __( get_bloginfo( 'url' ), 'wc-gateway-espiralapp' ),
				'desc_tip'    => true,
			),
		);
	}

	public function payment_fields(): void {
		include_once( 'templates/cardPayment3d.php' );
	}

	/**
	 * @throws Exception
	 */
	function ecommerceTransaction( $idOrder, $order ) {

		if ( $this->enabledProduction === 'yes' ) {
			User::setApiKey( $this->productionApiKey );
		} else {
			Environment::setEnvironment( 'develop' );
			EspiralApp::init();
			User::setApiKey( $this->sandboxApiKey );
		}

		$cardHolder = array(
			'cardHolder' => array(
				'name'  => '',
				'email' => $order->get_billing_email(),
				'phone' => $order->get_billing_phone(),
			)
		);

		$address = array(
			'address' => array(
				'country'   => $order->get_billing_country(),
				'state'     => $order->get_billing_state(),
				'city'      => $order->get_billing_city(),
				'numberExt' => 'NA',
				'numberInt' => '',
				'zipCode'   => $order->get_billing_postcode(),
				'street'    => $order->get_billing_address_1(),
			)
		);

		$items = array();
		foreach ( $order->get_items() as $item ) {
			$product = $item->get_product();
			error_log( print_r( $product->get_name() . ' - $' . $product->get_price() . ' - ' . $item->get_quantity(), true ) );
			$items[] = array(
				'name'     => $product->get_name(),
				'price'    => $product->get_price(),
				'quantity' => $item->get_quantity()
			);
		}

		$transaction = array(
			'transaction' => array(
				'items'    => $items,
				'total'    => $order->get_total(),
				'currency' => $order->get_currency()
			),
		);

		if ( ! ( $order->get_currency() === 'MXN') ) {
			if ( ! isset( $response['token'] ) ) {
				throw new Exception( 'Solo se permiten MXN' );
			}
		}

		$linkDetails = array(
			'linkDetails' => array(
				'name'            => 'link - ' . $idOrder,
				'email'           => $order->get_billing_email(),
				'reusable'        => true,
				'enableCard'      => true,
				'enableSpei'      => true,
				'enableReference' => true,
				'securityType3D'  => true,
				'enableCodi'      => true,
				'bank'            => 2
			)
		);

		$webhook = array(
			'webhook' => array(
				'redirectUrl' => $this->redirectSuccess,
      	'backPage' => get_bloginfo('url'),
				'redirectData' => array (
					'url' => get_bloginfo('url') . '/?wc-api=espiralresponsedata',
					'redirectMethod' => 'POST',
				)
			)
		);

		$metaData = array(
			'metadata' => array(
				'idOrder'					=> $idOrder
			)
		);

		$cart     = new Cart();
		$response = $cart->sign( $cardHolder, $address, $transaction, $linkDetails, $webhook, $metaData );

		if ( ! isset( $response['token'] ) ) {
			throw new Exception( 'Tarjeta declinada' . json_encode( $response ) );
		}

		return $response;
	}

	function process_payment( $order_id ) {
		global $woocommerce;
		$order = new WC_Order( $order_id );
		$idOrder = $order->get_id();
		try {
			$response = $this->ecommerceTransaction( $idOrder, $order );
			if ( $response ) {
				$order->update_status( 'on-hold' );

				$order->save();

				$config = Environment::getEnvironment();

				return array(
					'result'   => 'success',
					'redirect' => $this->enabledProduction === 'yes' ? $config['apiBaseCart'] . $response['token'] : $config['apiBaseCartReact'] . $response['token']
				);
			}
		} catch ( Exception $e ) {
			global $wp_version;

			if ( version_compare( $wp_version, '4.1', '>=' ) ) {
				wc_add_notice( __( 'Error: ', 'wc-gateway-espiralapp' ) . $e->getMessage(), $notice_type = 'error' );
			} else {
				$woocommerce->add_error( __( 'Error: ', 'wc-gateway-espiralapp' ) . $e->getMessage() );
			}

			$order->add_order_note( sprintf(
				"Error Transaction : '%s'",
				$e->getMessage()
			) );
		}
	}

	/**
	 * @throws Exception
	 */
	function ecommerceTransactionRefund( $order_id, $reference ) {
		/* Uncomment is only development */
		// \EspiralApp\Environment::setEnvironment('develop');
		// \EspiralApp\EspiralApp::init();

		if ( $this->enabledProduction === 'yes' ) {
			User::setApiKey( $this->productionApiKey );
		} else {
			User::setApiKey( $this->sandboxApiKey );
		}

		$cancel = new Cancel();

		$cancelTransaction = array(
			'saleTransaction' => array(
				'reference' => $reference
			)
		);

		$response = $cancel->cancel( $cancelTransaction );

		if (
			! isset( $response['codigo'] ) ||
			$response['codigo'] !== 200 ||
			! isset( $response['mensaje'] ) ||
			$response['mensaje'] !== 'Aprobada'
		) {
			if ( isset( $response['message'] ) ) {
				throw new Exception( $response['message'] );
			}
			throw new Exception( 'Error al realizar la cancelación' );
		}

		return $response;
	}

	public function process_refund( $order_id, $amount = null, $reason = '' ) {
		$order     = new WC_Order( $order_id );
		$orderData = $order->get_data();
		$reference = get_post_meta( $order_id, 'reference', true );

		if ( $orderData['total'] !== $amount ) {
			return new WP_Error( 'error', __( 'Error de cancelacion: El monto debe ser igual al monto de la orden.',
				'wc-gateway-espiralapp' ) );
		}

		try {
			$response = $this->ecommerceTransactionRefund( $order->get_id(), $reference );

			if ( $response ) {

				$order->add_order_note( sprintf(
					"Cancellation: %s,<br/>Reference: %s,<br/>Authorization Code: %s",
					$response['data']['transaction']['transactionId'],
					$response['data']['transaction']['reference'],
					$response['data']['transaction']['authCode']
				) );

				update_post_meta( $order->get_id(), 'cancellationTransactionId', $response['data']['transaction']['transactionId'] );
				update_post_meta( $order->get_id(), 'cancellationReference', $response['data']['transaction']['reference'] );
				update_post_meta( $order->get_id(), 'cancellationAuthCode', $response['data']['transaction']['authCode'] );

				return true;
			}

			return new WP_Error( 'error', __( 'Error al cancelar la transacción', 'wc-gateway-espiralapp' ) );
		} catch ( Exception $e ) {
			global $wp_version;

			$order->add_order_note( sprintf(
				"Error Transaction : '%s'",
				$e->getMessage()
			) );

			return new WP_Error( 'error', __( 'Error: ' . $e->getMessage(), 'wc-gateway-espiralapp' ) );
		}
	}

	/**
	 * Output for the order received page.
	 */
	public function thankyou_page() {
		if ( $this->instructions ) {
			echo wpautop( wptexturize( $this->instructions ) );
		}
	}

	public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {
		if ( $this->instructions && ! $sent_to_admin && 'belugapay3dsecurepayment' === $order->payment_method && $order->has_status( 'on-hold' ) ) {
			echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
		}
	}
}

